import Vue from 'vue'
import App from './App.vue'
import LoadScript from 'vue-plugin-load-script'
import './assets/sass/app.scss'

Vue.use(LoadScript)
Vue.loadScript('/bmah/js/jquery.min.js')
Vue.loadScript('/bmah/js/power.js')
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
