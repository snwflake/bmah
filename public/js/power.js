if (void 0 === $TauriPower) var $TauriPower = new function() {
    function t(t, e) {
        var o = document.createElement(t);
        return e && n(o, e), o
    }

    function e(t, e) {
        return t.appendChild(e)
    }

    function n(t, e) {
        for (var o in e) "object" == typeof e[o] ? (t[o] || (t[o] = {}), n(t[o], e[o])) : t[o] = e[o]
    }

    function o(t) {
        return t || (t = event), t._button || (t._button = t.which ? t.which : t.button, t._target = t.target ? t.target : t.srcElement), t
    }

    function i() {
        var t = 0,
            e = 0;
        return "number" == typeof window.pageYOffset ? (t = window.pageXOffset, e = window.pageYOffset) : document.body && (document.body.scrollLeft || document.body.scrollTop) ? (t = document.body.scrollLeft, e = document.body.scrollTop) : document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop) && (t = document.documentElement.scrollLeft, e = document.documentElement.scrollTop), {
            x: t,
            y: e
        }
    }

    function s(t) {
        var e = s.L;
        return e[t] ? e[t] : 0
    }

    function r(t) {
        var e = r.L;
        return e[t] ? e[t] : -1
    }

    function l(n) {
        e(I, t("script", {
            type: "text/javascript",
            src: n
        }))
    }

    function a(t) {
        var e = function(t) {
            var e, n;
            if (window.innerHeight) e = t.pageX, n = t.pageY;
            else {
                var o = i();
                e = t.clientX + o.x, n = t.clientY + o.y
            }
            return {
                x: e,
                y: n
            }
        }(t);
        E = e.x, k = e.y
    }

    function c(t, e) {
        if ("A" != t.nodeName && "AREA" != t.nodeName) return -2323;
        if (t.href.length) {
            var n, o, i, c, u, m, f = {};
            if (1 & P.applyto && (n = 1, o = 3, i = 4, c = t.href.match(/^http.?:\/\/(.+?)?\.?(|wotlk-|cata-|mop-)shoot\.tauri\.hu\/\?(item|quest|spell|achievement|npc|object|profile|pet-ability)=([^&#]+)/), H = 0), u = c, null == c && 2 & P.applyto && t.rel && (n = 0, o = 1, i = 2, (c = t.rel.match(/(item|quest|spell|achievement|npc|object|profile|pet-ability)=([0-9]+)/)) || (c = t.rel.match(/(profile)=([^&#]+)/)), H = 1), t.rel && (t.rel.replace(/([a-zA-Z]+)=?([a-zA-Z0-9:-]*)/g, function(t, e, n) {
                    "buff" == e || "sock" == e ? f[e] = !0 : "rand" == e || "ench" == e || "lvl" == e || "c" == e ? f[e] = parseInt(n) : "gems" == e || "pcs" == e ? f[e] = n.split(":") : "who" == e || "domain" == e ? f[e] = n : "when" == e && (f[e] = new Date(parseInt(n)))
                }), f.gems && f.gems.length > 0)) {
                var E;
                for (E = Math.min(3, f.gems.length - 1); E >= 0 && !parseInt(f.gems[E]); --E);
                0 == ++E ? delete f.gems : E < f.gems.length && (f.gems = f.gems.slice(0, E))
            }
            if (c) {
                var k, T = "www";
                var _h = c[0].split(":")[0] + ":";
                m = null === u ? "mop-" : c[1] ? c[1] : "wotlk-", f.domain ? T = f.domain : n && c[n] && (T = c[n]), k = s(T), "wotlk" == T && (T = "www"), T, t.onmousemove || (t.onmousemove = p, t.onmouseout = h), a(e),
                    function(t, e, n, o, i, _h) {
                        o || (o = {});
                        var s = i + w(e, o);
                        y = t, v = s, b = n, x = o, r = t, a = s, c = n, u = Z[r][0], null == u[a] && (u[a] = {}), null == u[a].status && (u[a].status = {}), null == u[a].status[c] && (u[a].status[c] = R);
                        var r, a, c, u;
                        var p = Z[t][0];
                        p[s].status[n] == X || p[s].status[n] == C ? d(p[s][g(n)], p[s].icon, i) : p[s].status[n] == z ? d(Y.tooltip_loading, 0, i) : function(t, e, n, o, i, s) {
                            var r = s + w(e, i),
                                a = Z[t][0];
                            if (a[r].status[n] == R || a[r].status[n] == B) {
                                a[r].status[n] = z, o || (a[r].timer = setTimeout(function() {
                                    (function(t, e, n, o) {
                                        if (y == t && v == e && b == n) {
                                            d(Y.loading, 0, o);
                                            var i = Z[t][0];
                                            i[e].timer = setTimeout(function() {
                                                (function(t, e, n, o) {
                                                    Z[t][0][e].status[n] = B, y == t && v == e && b == n && d(Y.tooltip_noresponse, 0, o)
                                                }).apply(this, [t, e, n, o])
                                            }, 3850)
                                        }
                                    }).apply(this, [t, r, n, s])
                                }, 333));
                                var c = "";
                                var _hh = (_h.match(/^http:/) ? "http": "https");
                                for (var u in i) "rand" != u && "ench" != u && "gems" != u && "sock" != u || (c += "object" == typeof i[u] ? "&" + u + "=" + i[u].join(":") : "sock" == u ? "&sock" : "&" + u + "=" + i[u]);
                                var p = "";
                                "mop-" == s ? (p += "https://mop-shoot.tauri.hu", l(p += "/ajax/ajax.php?" + Z[t][1] + "=" + e + "&power2" + c)) : "cata-" == s ? (p += "https://cata-shoot.tauri.hu", l(p += "/ajax.php?" + Z[t][1] + "=" + e + "&power2" + c)) : (p += "https://wotlk-shoot.tauri.hu", l(p += "/ajax.php?" + Z[t][1] + "=" + e + "&power2" + c))
                            }
                        }(t, e, n, null, o, i)
                    }(r(c[o]), c[i], k, f, m, _h)
            }
        }
    }

    function u(t) {
        for (var e = (t = o(t))._target, n = 0; null != e && n < 3 && -2323 == c(e, t);) e = e.parentNode, ++n
    }

    function p(t) {
        a(t = o(t)), f()
    }

    function h() {
        y = null, m()
    }

    function d(n, o, i) {
        var s = !1;
        if (T || function() {
                if (!T) {
                    var n = t("div"),
                        o = t("table"),
                        i = t("tbody"),
                        s = t("tr"),
                        r = t("tr"),
                        l = t("td"),
                        a = t("th"),
                        c = t("th"),
                        u = t("th");
                    n.className = "wowhead-tooltip", a.style.backgroundPosition = "top right", c.style.backgroundPosition = "bottom left", u.style.backgroundPosition = "bottom right", e(s, l), e(s, a), e(i, s), e(r, c), e(r, u), e(i, r), e(o, i), (A = t("p")).style.display = "none", e(A, t("div")), e(n, A), e(n, o), e(document.body, n), T = n, j = o, L = l, m()
                }
            }(), n) {
            if (null != x) {
                if (x.pcs && x.pcs.length) {
                    for (var r = 0, l = 0, a = x.pcs.length; l < a; ++l) {
                        var c;
                        (c = n.match(new RegExp("<span>\x3c!--si([0-9]+:)*" + x.pcs[l] + "(:[0-9]+)*--\x3e"))) && (n = n.replace(c[0], '<span class="q8">\x3c!--si' + x.pcs[l] + "--\x3e"), ++r)
                    }
                    r > 0 && (n = (n = n.replace("(0/", "(" + r + "/")).replace(new RegExp("<span>\\(([0-" + r + "])\\)", "g"), '<span class="q2">($1)'))
                }
                x.c && (n = (n = n.replace(/<span class="c([0-9]+?)">(.+?)<\/span><br \/>/g, '<span class="c$1" style="display: none">$2</span>')).replace(new RegExp('<span class="c(' + x.c + ')" style="display: none">(.+?)</span>', "g"), '<span class="c$1">$2</span><br />')), x.lvl && (n = n.replace(/\(<!--r([0-9]+):([0-9]+):([0-9]+)-->([0-9.%]+)(.+?)([0-9]+)\)/g, function(t, e, n, o, i, s, r) {
                    var l, a, c, u, p, h, d = (l = x.lvl, a = n, c = o, h = {
                        12: 1.5,
                        13: 12,
                        14: 15,
                        15: 5,
                        16: 10,
                        17: 10,
                        18: 8,
                        19: 14,
                        20: 14,
                        21: 14,
                        22: 10,
                        23: 10,
                        24: 0,
                        25: 0,
                        26: 0,
                        27: 0,
                        28: 10,
                        29: 10,
                        30: 10,
                        31: 10,
                        32: 14,
                        33: 0,
                        34: 0,
                        35: 25,
                        36: 10,
                        37: 2.5,
                        44: 4.69512176513672
                    }, l < 0 ? l = 1 : l > 80 && (l = 80), (14 == a || 12 == a || 15 == a) && l < 34 && (l = 34), c < 0 && (c = 0), null == h[a] ? u = 0 : (p = l > 70 ? 82 / 52 * Math.pow(131 / 63, (l - 70) / 10) : l > 60 ? 82 / (262 - 3 * l) : l > 10 ? (l - 8) / 52 : 2 / 52, u = c / h[a] / p), u);
                    return d = Math.round(100 * d) / 100, 12 != n && 37 != n && (d += "%"), "(\x3c!--r" + x.lvl + ":" + n + ":" + o + "--\x3e" + d + s + x.lvl + ")"
                })), x.who && x.when && (n = (n = n.replace("<table><tr><td><br />", '<table><tr><td><br /><span class="q2">' + sprintf(Y.tooltip_achievementcomplete, x.who, x.when.getMonth() + 1, x.when.getDate(), x.when.getFullYear()) + "</span><br /><br />")).replace(/class="q0"/g, 'class="r3"'))
            }
        } else n = Z[y][2] + " not found :(", o = "inv_misc_questionmark", s = !0;
        "wotlk-" != i && i || (i = ""), _ && (_.style.display = H && !s ? "" : "none"), M && o ? (A.style.backgroundImage = "url(https://" + i + "static.tauri.hu/images/icons/medium/" + o.toLowerCase() + ".png)", A.style.display = "") : (A.style.backgroundImage = "none", A.style.display = "none"), T.style.display = "", T.style.width = "320px", L.innerHTML = n,
            function() {
                var t = L.childNodes;
                if (t.length >= 2 && "TABLE" == t[0].nodeName && "TABLE" == t[1].nodeName) {
                    var e;
                    t[0].style.whiteSpace = "nowrap", (e = t[1].offsetWidth > 300 ? Math.max(300, t[0].offsetWidth) + 20 : Math.max(t[0].offsetWidth, t[1].offsetWidth) + 20) > 20 && (T.style.width = e + "px", t[0].style.width = t[1].style.width = "100%")
                } else T.style.width = j.offsetWidth + "px";
                var n = document.querySelector(".wowhead-tooltip .tooltip-table");
                n && (n.style.width = "100%")
            }(), f(), T.style.visibility = "visible"
    }

    function m() {
        T && (T.style.display = "none", T.style.visibility = "hidden")
    }

    function f() {
        if (T && null != E) {
            var t = (h = 0, d = 0, "number" == typeof window.innerWidth ? (h = window.innerWidth, d = window.innerHeight) : document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight) ? (h = document.documentElement.clientWidth, d = document.documentElement.clientHeight) : document.body && (document.body.clientWidth || document.body.clientHeight) && (h = document.body.clientWidth, d = document.body.clientHeight), {
                    w: h,
                    h: d
                }),
                e = i(),
                n = t.w,
                o = t.h,
                s = e.x,
                r = e.y,
                l = j.offsetWidth,
                a = j.offsetHeight,
                c = E + D,
                u = k - a - Q;
            if (c + D + l + 4 >= s + n) {
                var p = E - l - D;
                c = p >= 0 ? p : s + n - l - D - 4
            }
            u < r && ((u = k + Q) + a > r + o && (u = r + o - a, M && E >= c - 48 && E <= c && k >= u - 4 && k <= u + 48 && (u -= 48 - (k - u)))), T.style.left = c + "px", T.style.top = u + "px"
        }
        var h, d
    }

    function g(t) {
        return (x && x.buff ? "buff_" : "tooltip_") + F[t]
    }

    function w(t, e) {
        return t + (e.rand ? "r" + e.rand : "") + (e.ench ? "e" + e.ench : "") + (e.gems ? "g" + e.gems.join(",") : "") + (e.sock ? "s" : "")
    }
    s.L = {
        fr: 2,
        de: 3,
        es: 6,
        ru: 7,
        ptr: 25
    }, r.L = {
        npc: 1,
        object: 2,
        item: 3,
        itemset: 4,
        quest: 5,
        spell: 6,
        zone: 7,
        faction: 8,
        pet: 9,
        achievement: 10,
        profile: 100,
        "pet-ability": 200
    };
    var y, v, b, x, E, k, T, j, L, A, _, W, N, q, P = {
            applyto: 3
        },
        I = document.getElementsByTagName("head")[0],
        M = 1,
        H = 0,
        O = !(!window.attachEvent || window.opera),
        $ = -1 != navigator.userAgent.indexOf("MSIE 7.0"),
        S = -1 != navigator.userAgent.indexOf("MSIE 6.0") && !$,
        Y = {
            loading: "Loading...",
            noresponse: "No response from server :("
        },
        R = 0,
        z = 1,
        B = 2,
        C = 3,
        X = 4,
        D = 15,
        Q = 15,
        Z = {
            1: [{}, "npc", "NPC"],
            2: [{}, "object", "Object"],
            3: [{}, "item", "Item"],
            5: [{}, "quest", "Quest"],
            6: [{}, "spell", "Spell"],
            10: [{}, "achievement", "Achievement"],
            100: [{}, "profile", "Profile"],
            200: [{}, "pet-ability", "Pet Ability"]
        },
        F = {
            0: "enus",
            2: "frfr",
            3: "dede",
            6: "eses",
            7: "ruru",
            25: "ptr"
        };
    this.register = function(t, e, o, i, s) {
        var r = Z[t][0];
        s || (s = "wotlk"), e = s + "-" + e, clearTimeout(r[e].timer), n(r[e], i), r[e][g(o)] ? r[e].status[o] = X : r[e].status[o] = C, y == t && e == v && b == o && d(r[e][g(o)], r[e].icon, s + "-")
    }, this.registerNpc = function(t, e, n, o) {
        this.register(1, t, e, n, o)
    }, this.registerObject = function(t, e, n, o) {
        this.register(2, t, e, n, o)
    }, this.registerItem = function(t, e, n, o) {
        this.register(3, t, e, n, o)
    }, this.registerQuest = function(t, e, n, o) {
        this.register(5, t, e, n, o)
    }, this.registerSpell = function(t, e, n, o) {
        this.register(6, t, e, n, o)
    }, this.registerAchievement = function(t, e, n, o) {
        this.register(10, t, e, n, o)
    }, this.registerProfile = function(t, e, n, o) {
        this.register(100, t, e, n, o)
    }, this.registerPetAbility = function(t, e, n, o) {
        this.register(200, t, e, n, o)
    }, this.set = function(t) {
        n(P, t)
    }, this.showTooltip = function(t, e, n, o) {
        a(t), d(e, n, o + "-")
    }, this.hideTooltip = function() {
        m()
    }, this.moveTooltip = function(t) {
        p(t)
    }, e(I, t("link", {
        type: "text/css",
        href: I.baseURI.split(":")[0]+"://mop-shoot.tauri.hu/power/power.css",
        rel: "stylesheet"
    })), O && (e(I, t("link", {
        type: "text/css",
        href: I.baseURI.split(":")[0]+"://mop-shoot.tauri.hu/power/power_ie.css",
        rel: "stylesheet"
    })), S && e(I, t("link", {
        type: "text/css",
        href: I.baseURI.split(":")[0]+"://mop-shoot.tauri.hu/power/power_ie6.css",
        rel: "stylesheet"
    }))), W = document, N = "mouseover", q = u, window.attachEvent ? W.attachEvent("on" + N, q) : W.addEventListener(N, q, !1)
};